(App = (function() {
    $('#bar').click(function() {
        $(this).toggleClass('open');
        $('#page-content-wrapper ,#sidebar-wrapper, #top-header, #footer').toggleClass('toggled');

    });
    $('.left-sidebar-scroll').perfectScrollbar();
})()),
(App = (function() {
    $('#datatable').DataTable();
})()),
(App = (function() {

})()),
(App = (function() {

})());


// Sweetalert 2
var uiSweetalert2 = function() {
    (Swal = Swal.mixin({
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: "btn btn-secondary",
        buttonsStyling: !1,
    })),
    $("#swal-basic").click(function() {
            return (
                Swal.fire({
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis.",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                }), !1
            );
        }),
        $("#swal-title").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert example",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis.",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                }), !1
            );
        }),
        $("#swal-icon").click(function() {
            return (
                Swal.fire({
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis.",
                    type: "question",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    customClass: "content-text-center",
                }), !1
            );
        }),
        $("#swal-long").click(function() {
            return (
                Swal.fire({
                    imageUrl: "https://placeholder.pics/svg/300x1500",
                    imageHeight: 1500,
                    imageAlt: "A tall image",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                }), !1
            );
        }),
        $("#swal-tr").click(function() {
            return (
                Swal.fire({
                    position: "top-end",
                    type: "success",
                    title: "Sweetalert example",
                    showConfirmButton: !1,
                    timer: 1500,
                    customClass: "content-header-center",
                }), !1
            );
        }),
        $("#swal-tl").click(function() {
            return (
                Swal.fire({
                    position: "top-start",
                    type: "success",
                    title: "Sweetalert example",
                    showConfirmButton: !1,
                    timer: 1500,
                    customClass: "content-header-center",
                }), !1
            );
        }),
        $("#swal-bl").click(function() {
            return (
                Swal.fire({
                    position: "bottom-start",
                    type: "success",
                    title: "Sweetalert example",
                    showConfirmButton: !1,
                    timer: 1500,
                    customClass: "content-header-center",
                }), !1
            );
        }),
        $("#swal-br").click(function() {
            return (
                Swal.fire({
                    position: "bottom-end",
                    type: "success",
                    title: "Sweetalert example",
                    showConfirmButton: !1,
                    timer: 1500,
                    customClass: "content-header-center",
                }), !1
            );
        }),
        $("#swal-close").click(function() {
            return (
                Swal.fire({
                    title: "Close example",
                    type: "info",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    focusConfirm: !1,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
                    confirmButtonAriaLabel: "Thumbs up, great!",
                }), !1
            );
        }),
        $("#swal-success").click(function() {
            return (
                Swal.fire({
                    title: "Custom background.",
                    text: "This is a sweetalert with custom background",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "modal-full-color modal-full-color-success",
                }), !1
            );
        }),
        $("#swal-primary").click(function() {
            return (
                Swal.fire({
                    title: "Custom background.",
                    text: "This is a sweetalert with custom background",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "modal-full-color modal-full-color-primary",
                }), !1
            );
        }),
        $("#swal-warning").click(function() {
            return (
                Swal.fire({
                    title: "Custom background.",
                    text: "This is a sweetalert with custom background",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "modal-full-color modal-full-color-warning",
                }), !1
            );
        }),
        $("#swal-danger").click(function() {
            return (
                Swal.fire({
                    title: "Custom background.",
                    text: "This is a sweetalert with custom background",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "modal-full-color modal-full-color-danger",
                }), !1
            );
        }),
        $("#swal-dark").click(function() {
            return (
                Swal.fire({
                    title: "Custom background.",
                    text: "This is a sweetalert with custom background",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "modal-full-color modal-full-color-dark",
                }), !1
            );
        }),
        $("#swal-col-success").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert colored header.",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.",
                    confirmButtonText: "Proceed",
                    confirmButtonClass: "btn btn-success",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "colored-header colored-header-success",
                }), !1
            );
        }),
        $("#swal-col-primary").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert colored header.",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.",
                    confirmButtonText: "Proceed",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "colored-header colored-header-primary",
                }), !1
            );
        }),
        $("#swal-col-warning").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert colored header.",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.",
                    confirmButtonText: "Proceed",
                    confirmButtonClass: "btn btn-warning",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "colored-header colored-header-warning",
                }), !1
            );
        }),
        $("#swal-col-danger").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert colored header.",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.",
                    confirmButtonText: "Proceed",
                    confirmButtonClass: "btn btn-danger",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "colored-header colored-header-danger",
                }), !1
            );
        }),
        $("#swal-col-dark").click(function() {
            return (
                Swal.fire({
                    title: "Sweetalert colored header.",
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.",
                    confirmButtonText: "Proceed",
                    confirmButtonClass: "btn btn-dark",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    customClass: "colored-header colored-header-dark",
                }), !1
            );
        }),
        $("#swal-auto").click(function() {
            var e = 0;
            return (
                Swal.fire({
                    title: "Auto close alert!",
                    html: "I will close in <strong></strong> seconds.",
                    timer: 2e3,
                    customClass: "content-actions-center",
                    buttonsStyling: !0,
                    onOpen: function() {
                        swal.showLoading(),
                            (e = setInterval(function() {
                                swal
                                    .getContent()
                                    .querySelector(
                                        "strong"
                                    ).textContent = swal.getTimerLeft();
                            }, 100));
                    },
                    onClose: function() {
                        clearInterval(e);
                    },
                }).then(function(e) {
                    e.dismiss === swal.DismissReason.timer &&
                        console.log("I was closed by the timer");
                }), !1
            );
        }),
        $("#swal-queue").click(function() {
            return (
                Swal.mixin({
                    input: "text",
                    confirmButtonText: "Next &rarr;",
                    showCancelButton: !0,
                    customClass: "content-header-center content-header-title-left content-actions-left",
                    progressSteps: ["1", "2", "3"],
                })
                .queue(["Question 1", "Question 2", "Question 3"])
                .then(function(e) {
                    e.value &&
                        Swal.fire({
                            title: "All done!",
                            html: "Your answers: <pre><code>" +
                                JSON.stringify(e.value) +
                                "</code></pre>",
                            confirmButtonText: "Lovely!",
                        });
                }), !1
            );
        }),
        $("#swal-image").click(function() {
            return (
                Swal.fire({
                    title: "Sweet!",
                    text: "Modal with a custom image.",
                    confirmButtonText: "Proceed",
                    imageUrl: "https://unsplash.it/400/200",
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: "Custom image",
                }), !1
            );
        }),
        $("#swal-html").click(function() {
            return (
                Swal.fire({
                    title: "<strong>HTML <u>example</u></strong>",
                    type: "info",
                    html: 'You can use <b>bold text</b>, <a href="//github.com">links</a> and other HTML tags',
                    showCloseButton: !0,
                    showCancelButton: !0,
                    focusConfirm: !1,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
                    confirmButtonAriaLabel: "Thumbs up, great!",
                    customClass: "content-text-center",
                }), !1
            );
        }),
        $("#swal-input").click(function() {
            return (
                Swal.fire({
                    title: "Enter anything",
                    input: "text",
                    confirmButtonText: "Proceed",
                    showCancelButton: !0,
                    inputValidator: function(e) {
                        return !e && "You need to write something!";
                    },
                }), !1
            );
        }),
        $("#swal-textarea").click(function() {
            return (
                Swal.fire({
                    title: "Enter anything",
                    confirmButtonText: "Proceed",
                    input: "textarea",
                    inputPlaceholder: "Type your message here...",
                    showCancelButton: !0,
                    inputValidator: function(e) {
                        return !e && "You need to write something!";
                    },
                }), !1
            );
        }),
        $("#swal-select").click(function() {
            return (
                Swal.fire({
                    title: "Select country",
                    confirmButtonText: "Proceed",
                    input: "select",
                    inputOptions: { SRB: "Serbia", UKR: "Ukraine", HRV: "Croatia" },
                    inputPlaceholder: "Select country",
                    showCancelButton: !0,
                }), !1
            );
        }),
        $("#swal-radio").click(function() {
            return (
                Swal.fire({
                    title: "Select color",
                    confirmButtonText: "Proceed",
                    input: "radio",
                    inputOptions: {
                        "#ff0000": "Red",
                        "#00ff00": "Green",
                        "#0000ff": "Blue",
                    },
                    inputValidator: function(e) {
                        return !e && "You need to choose something!";
                    },
                }), !1
            );
        }),
        $("#swal-checkbox").click(function() {
            return (
                Swal.fire({
                    title: "Terms and conditions",
                    confirmButtonText: "Proceed",
                    input: "checkbox",
                    inputValue: 1,
                    inputPlaceholder: "I agree with the terms and conditions",
                    inputValidator: function(e) {
                        return !e && "You need to agree with T&C";
                    },
                }), !1
            );
        }),
        $("#swal-file").click(function() {
            return (
                Swal.fire({
                    title: "Select image",
                    confirmButtonText: "Proceed",
                    input: "file",
                    inputAttributes: {
                        accept: "image/*",
                        "aria-label": "Upload your profile picture",
                    },
                }), !1
            );
        });
};
uiSweetalert2();


// Notifications
var uiNotifications = function() {
    conf = {

        assetsPath: "assets/images",
        imgPath: "avatar"
    }
    var assetsPath = "assets";
    var imgPath = "images";

    $("#not-theme").click(function() {
            return (
                $.gritter.add({
                    title: "Welcome home!",
                    text: "You can start your day checking the new messages.",
                    image: conf.assetsPath + "/" + conf.imgPath + "/avatar5.png",
                    class_name: "clean img-rounded",
                    time: "",
                }), !1
            );
        }),
        $("#not-sticky").click(function() {
            return (
                $.gritter.add({
                    title: "Sticky Note",
                    text: "Your daily goal is 130 new code lines, don't forget to update your work.",
                    image: conf.assetsPath +
                        "/" +
                        conf.imgPath +
                        "/slack_logo.png",
                    class_name: "clean",
                    sticky: !0,
                    time: "",
                }), !1
            );
        }),
        $("#not-text").click(function() {
            return (
                $.gritter.add({
                    title: "Just Text",
                    text: "This is a simple Gritter Notification. Etiam efficitur efficitur nisl eu dictum, nullam non orci elementum.",
                    class_name: "clean",
                    time: "",
                }), !1
            );
        }),
        $("#not-tr").click(function() {
            return (
                $.extend($.gritter.options, { position: "top-right" }),
                $.gritter.add({
                    title: "Top Right",
                    text: "This is a simple Gritter Notification. Etiam efficitur efficitur nisl eu dictum, nullam non orci elementum",
                    class_name: "clean",
                }), !1
            );
        }),
        $("#not-tl").click(function() {
            return (
                $.extend($.gritter.options, { position: "top-left" }),
                $.gritter.add({
                    title: "Top Left",
                    text: "This is a simple Gritter Notification. Etiam efficitur efficitur nisl eu dictum, nullam non orci elementum",
                    class_name: "clean",
                }), !1
            );
        }),
        $("#not-bl").click(function() {
            return (
                $.extend($.gritter.options, { position: "bottom-left" }),
                $.gritter.add({
                    title: "Bottom Left",
                    text: "This is a simple Gritter Notification. Etiam efficitur efficitur nisl eu dictum, nullam non orci elementum",
                    class_name: "clean",
                }), !1
            );
        }),
        $("#not-br").click(function() {
            return (
                $.extend($.gritter.options, { position: "bottom-right" }),
                $.gritter.add({
                    title: "Bottom Right",
                    text: "This is a simple Gritter Notification. Etiam efficitur efficitur nisl eu dictum, nullam non orci elementum",
                    class_name: "clean",
                }), !1
            );
        }),
        $("#not-primary").click(function() {
            $.gritter.add({
                title: "Primary",
                text: "This is a simple Gritter Notification.",
                class_name: "color primary",
            });
        }),
        $("#not-success").click(function() {
            $.gritter.add({
                title: "Success",
                text: "This is a simple Gritter Notification.",
                class_name: "color success",
            });
        }),
        $("#not-warning").click(function() {
            $.gritter.add({
                title: "Warning",
                text: "This is a simple Gritter Notification.",
                class_name: "color warning",
            });
        }),
        $("#not-danger").click(function() {
            $.gritter.add({
                title: "Danger",
                text: "This is a simple Gritter Notification.",
                class_name: "color danger",
            });
        }),
        $("#not-dark").click(function() {
            $.gritter.add({
                title: "Dark Color",
                text: "This is a simple Gritter Notification.",
                class_name: "color dark",
            });
        });
}
uiNotifications();

var formElements = function() {
    $(".datetimepicker").datetimepicker({
            autoclose: !0,
            componentIcon: ".far.fa-calendar-alt",
            navIcons: {
                rightIcon: "fas fa-chevron-right",
                leftIcon: "fas fa-chevron-left",
            },
        }),
        $(".select2").select2({
            width: "100%",
            placeholder: "Select a state",
        }),
        $(".js-select2").select2({
            closeOnSelect: false,
            placeholder: "Placeholder",
            allowHtml: true,
            allowClear: true,
            tags: true // создает новые опции на лету
        });
    $(".tags").select2({ tags: !0, width: "100%" }),
        $("#editor1").summernote({ height: 300 }),
        $('#multiselect').multiselect({
            buttonWidth: '100%',
            buttonClass: 'btn btn-default form-multiple',
        })

}
formElements();